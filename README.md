# WDIA

WDIA website

## Public urls


dev: [https://wdia.org/](https://wdia.org/)


## Installation

Use the package manager [yarn](https://yarnpkg.com/) to install multipass.

```bash
yarn
```

## Run app in dev mode

```bash
yarn dev
```

Runs the app in the development mode.
Open http://localhost:3000 to view it in the browser.

## Build

```bash
yarn build
```

## Start build

```bash
yarn start
```

## Fix code style

```bash
yarn format
```

## Start tests

```bash
yarn test
```

## Docker
1. Run in Docker locally:
```
NODE_ENV=development HOST=0.0.0.0 PORT=7005 docker-compose up -d --build
```
2. Tests:
```
curl 0.0.0.0:7005
```
