import moment from "moment";
import { useTranslation } from "react-i18next";

export const getDate = (date) => {
  return moment(date);
};

export const renderDate = (date, format = "DD.MM.YYYY") => {
  const { i18n } = useTranslation();

  const getDate = moment(date).locale(i18n.language);
  return getDate.format(format);
};
