export const renderNumber = (number, local, maximumFractionDigits = 2) => {
  return new Intl.NumberFormat(local, {
    maximumFractionDigits,
  }).format(number);
};
