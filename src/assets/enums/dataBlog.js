import { useTranslation } from "react-i18next";

export const dataBlog = () => {
  const { t } = useTranslation("blog");

  return [
    {
      img: "/static/img/_general/blog/item-1.jpg",
      title: t("blog-1-title"),
      description: t("blog-1-text"),
      date: new Date("2020.02.03"),
      moreLink:
        "https://medium.com/@wdia.org/aml-kyc-problem-in-the-digital-world-43ddc81eb169",
    },
    {
      img: "/static/img/_general/blog/item-2.jpg",
      title: t("blog-2-title"),
      description: t("blog-2-text"),
      date: new Date("2020.02.14"),
      moreLink:
        "https://medium.com/@wdia.org/time-is-our-most-precious-resource-6b7e36c1e4f2",
    },
    {
      img: "/static/img/_general/blog/item-3.jpg",
      title: t("blog-3-title"),
      description: t("blog-3-text"),
      date: new Date("2020.02.18"),
      moreLink:
        "https://medium.com/@wdia.org/digital-id-and-blockchain-9d4a596c7b56",
    },
    {
      img: "/static/img/_general/blog/item-4.jpg",
      title: t("blog-4-title"),
      description: t("blog-4-text"),
      date: new Date("2020.02.20"),
      moreLink:
        "https://medium.com/@wdia.org/financial-standards-for-digital-id-were-released-da6f4dd92224",
    },
    {
      img: "/static/img/_general/blog/item-5.jpg",
      title: t("blog-5-title"),
      description: t("blog-5-text"),
      date: new Date("2020.02.27"),
      moreLink:
        "https://medium.com/@wdia.org/how-digital-id-already-works-in-different-countries-b0d7c29a9556",
    },
    {
      img: "/static/img/_general/blog/item-6.jpg",
      title: t("blog-6-title"),
      description: t("blog-6-text"),
      date: new Date("2020.03.03"),
      moreLink:
        "https://medium.com/@wdia.org/when-do-they-start-boiling-or-how-many-minutes-tourists-can-tolerate-at-hotel-registration-desks-67449c069146",
    },
    {
      img: "/static/img/_general/blog/item-7.jpg",
      title: t("blog-7-title"),
      description: t("blog-7-text"),
      date: new Date("2020.03.17"),
      moreLink:
        "https://medium.com/@wdia.org/types-of-biometric-personality-verification-e89928e22dba",
    },
  ].map((item, i) => ({ ...item, id: i }));
};
