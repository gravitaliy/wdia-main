export const routesList = {
  ABOUT_US: "/about-us",
  ABOUT_TECH: "/about-tech",
  OUR_ACTIVITIES: "/our-activities",
  JOIN_US: "/join-us",
  OUR_NETWORK: "/our-network",
  MEMBERSHIP: "/membership",
  CONTACTS: "/join-us",
};
