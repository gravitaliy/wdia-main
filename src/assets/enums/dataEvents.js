import { useTranslation } from "react-i18next";
import { routesList } from "./routesList";

export const dataEvents = () => {
  const { t } = useTranslation("events");

  return [
    {
      imgLg: "/static/img/_general/events/item-1/item-1-1115.jpg",
      imgMd: "/static/img/_general/events/item-1/item-1-540.jpg",
      imgSm: "/static/img/_general/events/item-1/item-1-350.jpg",
      title: t("event-1-title"),
      date: new Date("2020.01.21"),
      contentStr: t("event-1-content"),
    },
    {
      imgLg: "/static/img/_general/events/item-3/item-3-1115.jpg",
      imgMd: "/static/img/_general/events/item-3/item-3-540.jpg",
      imgSm: "/static/img/_general/events/item-3/item-3-350.jpg",
      title: t("event-2-title"),
      date: new Date("2020.02.06"),
      contentStr: t("event-2-content"),
    },
    {
      imgLg: "/static/img/_general/events/item-2/item-2-1115.jpg",
      imgMd: "/static/img/_general/events/item-2/item-2-540.jpg",
      imgSm: "/static/img/_general/events/item-2/item-2-350.jpg",
      title: t("event-3-title"),
      date: new Date("2019.12.18"),
      contentStr: t("event-3-content"),
    },
  ].map((item, i) => ({
    ...item,
    id: i,
    type: t("type"),
    showAll: `${routesList.OUR_ACTIVITIES}#events`,
    href: `${routesList.OUR_ACTIVITIES}/`,
  }));
};
