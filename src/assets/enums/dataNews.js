import { useTranslation } from "react-i18next";
import { routesList } from "./routesList";

export const dataNews = () => {
  const { t } = useTranslation("news");

  return [
    {
      img: "/static/img/_general/news/1.jpg",
      title: t("news-1-title"),
      description: t("news-1-text"),
      date: new Date("2020.03.11"),
      link: "https://medium.com/@wdia.org/wdia-team-in-istanbul-e6cf5e354a1d",
    },
    {
      img: "/static/img/_general/news/2.jpg",
      title: t("news-2-title"),
      description: t("news-2-text"),
      date: new Date("2020.02.11"),
      link:
        "https://medium.com/@wdia.org/a-new-important-partner-of-wdia-6f2e538bcf5c",
    },
    {
      img: "/static/img/_general/news/3.jpg",
      title: t("news-3-title"),
      description: t("news-3-text"),
      date: new Date("2020.01.29"),
      link:
        "https://medium.com/@wdia.org/wdia-davos-2020-presentation-we-will-make-the-future-more-comfortable-c06719a14779",
    },
    {
      img: "/static/img/_general/news/4.jpg",
      title: t("news-4-title"),
      description: t("news-4-text"),
      date: new Date("2020.01.22"),
      link:
        "https://medium.com/@wdia.org/the-second-day-of-our-wdia-team-in-davos-is-in-full-swing-45d68dd0ff20",
    },
    {
      img: "/static/img/_general/news/5.jpg",
      title: t("news-5-title"),
      description: t("news-5-text"),
      date: new Date("2020.01.21"),
      link:
        "https://medium.com/@wdia.org/wdia-at-wef-2020-in-davos-d600a3cd6b6e",
    },
    {
      img: "/static/img/_general/news/6.jpg",
      title: t("news-6-title"),
      description: t("news-6-text"),
      date: new Date("2019.12.21"),
      link:
        "https://medium.com/@wdia.org/personal-data-wdia-meetup-2019-in-geneva-marks-noticeable-progress-in-kyc-community-development-8700762fbc52",
    },
  ].map((item, i) => ({
    ...item,
    id: i,
    type: t("news"),
    showAll: `${routesList.OUR_ACTIVITIES}#news`,
  }));
};
