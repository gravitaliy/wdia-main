import React from "react";
import { ButtonMore } from "../../../../components/Buttons/ButtonMore/ButtonMore";
import styles from "./Description.module.scss";

export const Description = ({ t }) => {
  return (
    <section className="c-section-offset">
      <div className="c-container">
        <div className="c-title c-text-gradient">{t("description-title")}</div>
        <div className={styles["description-text"]}>
          <p>{t("description-text-1")}</p>
          <p>{t("description-text-2")}</p>
        </div>
        <ButtonMore
          href="//create.multipass.org/"
          target="_blank"
          rel="noopener noreferrer"
          color="blue"
          center
          size="lg"
          title={t("common:btn-create-multipass")}
        />
      </div>
    </section>
  );
};
