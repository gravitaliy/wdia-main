import React from "react";
import clsx from "clsx";
import styles from "./Digital.module.scss";

export const Digital = ({ t }) => {
  const digitalData = [
    {
      img: "/static/img/AboutTech/Digital/item-1.jpg",
      text: t("digital-item-1"),
    },
    {
      img: "/static/img/AboutTech/Digital/item-2.jpg",
      text: t("digital-item-2"),
    },
  ].map((item, i) => ({ ...item, id: i }));

  return (
    <section className={clsx(styles.digital, "c-section-offset")}>
      <div className="c-container">
        <div className={styles["digital-inner"]}>
          <div className="c-title c-text-gradient">{t("digital-title")}</div>
          <div className={styles.digital__items}>
            {digitalData.map((item) => (
              <div className={styles["digital__items-item"]} key={item.id}>
                <div className={styles["digital__items-item__img-wrap"]}>
                  <img className="fix-img img-fit" src={item.img} alt="" />
                </div>
                <div className={styles["digital__items-item__text"]}>
                  {item.text}
                </div>
              </div>
            ))}
          </div>
        </div>
      </div>
    </section>
  );
};
