import React from "react";
import clsx from "clsx";
import styles from "./Believe.module.scss";

export const Believe = ({ t }) => {
  return (
    <section className={clsx(styles.believe, "c-section-offset")}>
      <div className="c-container">
        <div className={styles["believe-inner"]}>
          <div className={styles.believe__row}>
            <div className={styles.believe__left}>
              <div className={clsx(styles.believe__left__title, "c-title")}>
                {t("believe-title")}
              </div>
              <div className={styles.believe__left__description}>
                <p>{t("believe-text-1")}</p>
                <p>{t("believe-text-2")}</p>
              </div>
            </div>
            <div className={styles.believe__right}>
              <img
                className="fix-img"
                src="/static/img/AboutTech/Believe/img.png"
                alt=""
              />
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};
