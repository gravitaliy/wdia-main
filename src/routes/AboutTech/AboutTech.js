import React from "react";
import { useTranslation } from "react-i18next";
import { Breadcrumbs } from "../../components/Breadcrumbs/Breadcrumbs";
import { PromoPage } from "../../components/PromoPage/PromoPage";
import { Digital } from "./components/Digital/Digital";
import { Believe } from "./components/Believe/Believe";
import { AboutCardsWrap } from "../../components/Cards/AboutCardsWrap/AboutCardsWrap";
import { Description } from "./components/Description/Description";

const breadcrumbsParentsData = [
  { label: "WDIA", href: "/" },
].map((item, i) => ({ ...item, id: i }));

export const AboutTech = () => {
  const { t } = useTranslation(["about-tech", "common"]);

  const dataAbout = [
    {
      img: "/static/img/AboutTech/about/item-1.jpg",
      title: t("about-item-1"),
    },
    {
      img: "/static/img/AboutTech/about/item-2.jpg",
      title: t("about-item-2"),
    },
    {
      img: "/static/img/AboutTech/about/item-3.jpg",
      title: t("about-item-3"),
    },
    {
      img: "/static/img/AboutTech/about/item-4.jpg",
      title: t("about-item-4"),
    },
    {
      img: "/static/img/AboutTech/about/item-5.jpg",
      title: t("about-item-5"),
    },
  ].map((item, i) => ({ ...item, id: i }));

  return (
    <main className="c-main">
      <Breadcrumbs
        parentsData={breadcrumbsParentsData}
        active={t("common:nav-1-1")}
      />
      <PromoPage title={t("promo-title")}>
        <p>{t("promo-text-1")}</p>
        <p>{t("promo-text-2")}</p>
      </PromoPage>
      <Digital t={t} />
      <Believe t={t} />
      <AboutCardsWrap title={t("about-title")} data={dataAbout} />
      <Description t={t} />
    </main>
  );
};
