import React from "react";
import clsx from "clsx";
import { InfoCards } from "../../../../components/Cards/InfoCards/InfoCards";
import { dataNews } from "../../../../assets/enums/dataNews";
import styles from "./News.module.scss";

export const News = ({ t }) => {
  return (
    <section className={clsx(styles.news, "c-section-offset")} id="news">
      <div className="c-container">
        <div className={styles["news-inner"]}>
          <div className="c-title">{t("news-title")}</div>
          <InfoCards data={dataNews()} sort="desc" />
        </div>
      </div>
    </section>
  );
};
