import React from "react";
import clsx from "clsx";
import { BlogCards } from "../../../../components/Cards/BlogCards/BlogCards";
import { dataBlog } from "../../../../assets/enums/dataBlog";
import styles from "./Blog.module.scss";

export const Blog = ({ t }) => {
  return (
    <section className={clsx(styles.blog, "c-section-offset")}>
      <div className="c-container">
        <div className={styles["blog-inner"]}>
          <div className="c-title">{t("blog-title")}</div>
          <BlogCards data={dataBlog()} sort="desc" />
        </div>
      </div>
    </section>
  );
};
