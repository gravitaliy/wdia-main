import React, { useState } from "react";
import clsx from "clsx";
import { EventsNewsCards } from "../../../../components/Cards/EventsNewsCards/EventsNewsCards";
import { dataEvents } from "../../../../assets/enums/dataEvents";
import styles from "./EventCalendar.module.scss";

export const EventCalendar = ({ t }) => {
  const [eventTime, setEventTime] = useState("past");
  const [count, setCount] = useState(0);

  const countLabel = () => {
    let label;
    switch (count) {
      case 0:
        label = t("no-event-label");
        break;
      case 1:
        label = `${count} ${t("one-event-label")}`;
        break;
      default:
        label = `${count} ${t("many-events-label")}`;
    }
    return label;
  };

  const onTabClick = time => () => {
    setEventTime(time);
  }

  const onSetCount = (n) => {
    setCount(n);
  };

  return (
    <section
      className={clsx(styles["event-calendar"], "c-section-offset")}
      id="events"
    >
      <div className="c-container">
        <div className={styles["event-calendar-inner"]}>
          <h3 className="c-title">{t("events-title")}</h3>
          <div>
            {/* ========== SHOW ONLY <SM ================== */}
            <div className="c-sm-show">
              <div className={styles["event-calendar__items-title-wrap"]}>
                <div
                  className={clsx(
                    styles["event-calendar__items-title"],
                    "c-text-gradient",
                  )}
                >
                  {countLabel()}
                </div>
                <div className={styles["event-calendar__items-tabs"]}>
                  <button
                    className={clsx(
                      styles["event-calendar__items-tabs-tab"],
                      "default-style",
                      eventTime === "past" && styles.active,
                    )}
                    type="button"
                    onClick={onTabClick("past")}
                  >
                    {t("event-past-label")}
                  </button>
                  <button
                    className={clsx(
                      styles["event-calendar__items-tabs-tab"],
                      "default-style",
                      eventTime === "future" && styles.active,
                    )}
                    type="button"
                    onClick={onTabClick("future")}
                  >
                    {t("event-upcoming-label")}
                  </button>
                </div>
              </div>
              <EventsNewsCards
                data={dataEvents()}
                type={eventTime}
                sort="asc"
                onSetCount={onSetCount}
              />
            </div>

            {/* ========== SHOW ONLY >SM ================== */}
            <div className="c-sm-hide">
              <EventsNewsCards data={dataEvents()} type="future" sort="asc" />
              <EventsNewsCards data={dataEvents()} type="past" sort="desc" />
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};
