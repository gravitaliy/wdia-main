import React from "react";
import styles from "./Promo.module.scss";

export const Promo = ({ t }) => {
  return (
    <section className={styles.promo}>
      <div className="c-container">
        <div className={styles["promo-inner"]}>
          <h3 className="c-title" style={{ marginBottom: 0 }}>
            {t("common:nav-2")}
          </h3>
        </div>
      </div>
    </section>
  );
};
