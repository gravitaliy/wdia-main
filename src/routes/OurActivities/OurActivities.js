import React from "react";
import { useTranslation } from "react-i18next";
import { Promo } from "./components/Promo/Promo";
import { EventCalendar } from "./components/EventCalendar/EventCalendar";
import { News } from "./components/News/News";
import { Blog } from "./components/Blog/Blog";

export const OurActivities = () => {
  const { t } = useTranslation(["our-activities", "common"]);

  return (
    <main className="c-main">
      <Promo t={t} />
      <EventCalendar t={t} />
      <News t={t} />
      <Blog t={t} />
    </main>
  );
};
