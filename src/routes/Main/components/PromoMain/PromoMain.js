import React from "react";
import clsx from "clsx";
import Link from "next/link";
import { Title } from "../../../../components/Typography/Title/Title";
import { Button } from "../../../../components/Buttons/Button/Button";
import { routesList } from "../../../../assets/enums/routesList";
import styles from "./PromoMain.module.scss";
// import { Slider } from "../../../../components/Slider/Slider";
// import { Slide } from "../../../../components/Slider/Slide";

export const PromoMain = ({ t }) => {
  return (
    <div className={styles["main-header"]}>
      <div className="c-container">
        <div className={styles["slider-content"]}>
          <div className={styles["slider-content__left"]}>
            <Title color="blue" className={styles["header-title"]}>
              {t("promo-title")}
            </Title>
            <p className={clsx(styles["header-paragraph"], "paragraph")}>
              {t("promo-description")}
            </p>
            <div
              className={styles["buttons-group-slide"]}
              style={{ marginTop: 30 }}
            >
              <Link href={routesList.MEMBERSHIP}>
                <a
                  style={{
                    textDecoration: "none",
                    color: "white",
                  }}
                  href={routesList.MEMBERSHIP}
                >
                  <Button style={{ marginRight: 20 }}>{t("btn-1")}</Button>
                </a>
              </Link>
              <a
                style={{ textDecoration: "none", color: "#1771f1" }}
                href="http://dev-admin.wdia.org/"
                target="_blank"
                rel="noopener noreferrer"
              >
                <Button typeButton="transparent">{t("btn-2")}</Button>
              </a>
            </div>
          </div>
          <div className={styles["slider-content__right"]}>
            <div className={styles["video-block-img-wrap"]}>
              <img
                className="fix-img"
                src="/static/img/Main/video.jpg"
                alt="video"
              />
              {/*
              <img
                className={styles["video-block-img-play"]}
                src="/static/img/Main/play.svg"
                alt="play"
              />
              */}
            </div>
          </div>
        </div>
        {/*
        <Slider>
          <Slide>{content()}</Slide>
          <Slide>{content()}</Slide>
          <Slide>{content()}</Slide>
        </Slider>
        */}
      </div>
    </div>
  );
};
