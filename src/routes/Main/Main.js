import React from "react";
import { useTranslation } from "react-i18next";
import clsx from "clsx";
import { PromoMain } from "./components/PromoMain/PromoMain";
import { CardInfo } from "../../components/Cards/CardInfo/CardInfo";
import { Button } from "../../components/Buttons/Button/Button";
import { CardSystem } from "../../components/Cards/CardSystem/CardSystem";
import { Title } from "../../components/Typography/Title/Title";
import { EventsNewsCards } from "../../components/Cards/EventsNewsCards/EventsNewsCards";
import { dataEvents } from "../../assets/enums/dataEvents";
import { MapInner } from "../../components/MapInner/MapInner";
import styles from "./Main.module.scss";

export const Main = () => {
  const { t } = useTranslation(["index", "common"]);

  return (
    <div>
      <PromoMain t={t} />
      <main>
        <section className={styles["main-about"]}>
          <div className="c-container">
            <div className="flex justify-center">
              <Title
                style={{ color: "white", fontSize: 45, textAlign: "center" }}
              >
                {t("about-title")}
              </Title>
            </div>
            <div className={styles["list-about-cards"]}>
              <CardInfo t={t} />
            </div>
          </div>
        </section>
        <section className={styles["main-ecosystem"]}>
          <div className="c-container">
            <div>
              <Title
                style={{
                  color: "#515151",
                  textAlign: "center",
                  fontSize: 45,
                  marginBottom: 20,
                }}
              >
                {t("ecosystem-title")}
              </Title>
              <p className={styles["secton-subtitle"]}>
                {t("ecosystem-description")}
              </p>
            </div>
            <div
              className={styles["list-cards-system"]}
              style={{ marginTop: 30 }}
            >
              <CardSystem t={t} />
            </div>
          </div>
        </section>
        <section className={styles["main-news"]}>
          <div className="c-container">
            <Title
              style={{
                textAlign: "center",
                color: "white",
                marginBottom: 40,
                fontSize: 45,
              }}
            >
              {t("news-events-title")}
            </Title>
            <div className={styles["list-cards-news"]}>
              <EventsNewsCards
                data={dataEvents()}
                sort="desc"
                variant="mix"
                theme="dark"
                t={t}
              />
            </div>
          </div>
        </section>
        <section className={styles["main-solution"]}>
          <div className="c-container">
            <div className={clsx(styles["media-container"], "flex")}>
              <div className={styles["media-container__left"]}>
                <p className={styles["text-gradient"]}>
                  {t("solution-title-1")} —
                </p>
                <Title style={{ color: "white", fontSize: 40 }}>
                  {t("solution-title-2")}
                </Title>
                <p className={styles["solution-text"]}>
                  {t("solution-text-1")}
                </p>
                <p className={styles["solution-text"]}>
                  {t("solution-text-2")}
                </p>
                <div className={styles["solution-buttons-group"]}>
                  <a
                    style={{ textDecoration: "none", color: "white" }}
                    href="//create.multipass.org"
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    <Button
                      style={{
                        padding: "12px 35px",
                        marginRight: 20,
                        marginBottom: 15,
                      }}
                    >
                      {t("common:btn-create-multipass")}
                    </Button>
                  </a>
                  <a
                    style={{ textDecoration: "none", color: "white" }}
                    href="https://multipass.org/"
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    <Button
                      style={{ padding: 12 }}
                      typeButton="transparent"
                      color="white"
                    >
                      {t("learn-more-btn")}
                    </Button>
                  </a>
                </div>
              </div>
              <div className={styles["media-container__right"]}>
                <img
                  className="fix-img"
                  src="/static/img/Main/solution.png"
                  alt="solution"
                />
              </div>
            </div>
          </div>
        </section>
        <section className={styles["main-map"]}>
          <div className="c-container">
            <div style={{ marginBottom: 50 }}>
              <Title
                className={styles["text-gradient"]}
                style={{
                  fontSize: 40,
                  textAlign: "center",
                  marginBottom: 20,
                  wordBreak: "break-word",
                }}
              >
                {t("map-title")}
              </Title>
              <p className={styles["secton-subtitle"]}>
                {t("map-description")}
              </p>
            </div>
            <MapInner />
          </div>
        </section>
      </main>
    </div>
  );
};
