import React from "react";
import { useTranslation } from "react-i18next";
import { Breadcrumbs } from "../../components/Breadcrumbs/Breadcrumbs";
import { Members } from "./components/Members/Members";
import { Participants } from "./components/Participants/Participants";
import { routesList } from "../../assets/enums/routesList";
import { renderNumber } from "../../assets/enums/renderNumber";

const breadcrumbsParentsData = [
  { label: "WDIA", href: "/" },
].map((item, i) => ({ ...item, id: i }));

export const Membership = () => {
  const { t, i18n } = useTranslation("membership");

  const renderNum = num => renderNumber(num, i18n.language);

  /* ==================================== TARIFS DATA ========================= */
  const tarifsData = [
    {
      title: t("tarifs-1-title"),
      price: renderNum(5000),
      fee: t("common-fee-entry"),
      features: [
        t("common-access"),
        t("common-vote-several"),
        t("tarifs-1-features-1"),
        `<b>25% ${t("common-discount-pre")}</b> ${t("common-discount")} 5 ${t(
          "common-discount-after",
        )}`,
        t("common-duration-membership"),
        `${t("common-annual")} <b>${renderNum(5000)} CHF</b>`,
        `${t("common-onboarding")} <b>${renderNum(250)} CHF</b>`,
      ],
    },
    {
      title: t("tarifs-2-title"),
      price: renderNum(25000),
      fee: t("common-fee-entry"),
      features: [
        t("common-access"),
        t("common-vote-several"),
        t("tarifs-2-features-1"),
        t("common-deligate"),
        `<b>50% ${t("common-discount-pre")}</b> ${t("common-discount")} 10 ${t(
          "common-discount-after",
        )}`,
        t("common-duration-membership"),
        `${t("common-annual")} <b>${renderNum(50000)} CHF</b>`,
        `${t("common-onboarding")} <b>${renderNum(250)} CHF</b>`,
      ],
    },
    {
      title: t("tarifs-3-title"),
      price: renderNum(25000),
      fee: t("common-fee-entry"),
      features: [
        t("common-access"),
        t("common-vote-several"),
        t("tarifs-3-features-1"),
        t("common-deligate"),
        t("tarifs-3-features-2"),
        `<b>50% ${t("common-discount-pre")}</b> ${t("common-discount")} 10 ${t(
          "common-discount-after",
        )}`,
        t("common-duration-membership"),
        `${t("common-annual")} <b>${renderNum(50000)} CHF</b>`,
        `${t("common-onboarding")} <b>${renderNum(250)} CHF</b>`,
      ],
    },
    {
      title: t("tarifs-4-title"),
      price: renderNum(2500),
      fee: t("common-fee-entry"),
      features: [
        t("common-access"),
        t("common-vote-every"),
        t("tarifs-4-features-1"),
        t("common-deligate"),
        t("tarifs-4-features-2"),
        `<b>15% ${t("common-discount-pre")}</b> ${t("common-discount")} 3 ${t(
          "common-discount-after",
        )}`,
        t("common-duration-membership"),
        `${t("common-annual")} <b>${renderNum(2500)} CHF</b>`,
        `${t("common-onboarding")} <b>${renderNum(250)} CHF</b>`,
      ],
    },
  ].map((item, i) => ({
    ...item,
    id: i,
    href: "/static/img/Membership/WDIA_Membership_Application_Form copy.pdf",
  }));
  /* ==================================== END TARIFS DATA ========================= */

  /* ==================================== PARTICIPANTS DATA ========================= */
  const participantsData = [
    {
      title: t("participants-1-title"),
      price: renderNum(2000),
      fee: t("common-fee-entry"),
      features: [
        t("common-access"),
        t("participants-1-features"),
        `${t("common-annual")} <b>${t("common-fee-na")}</b>`,
        `${t("common-onboarding")} <b>${renderNum(250)} CHF</b>`,
      ],
      href:
        "/static/img/Membership/WDIA_Application_Form_Candidate_Member copy.pdf",
    },
    {
      title: t("participants-2-title"),
      price: renderNum(250),
      fee: t("common-fee-annual"),
      features: [
        t("participants-2-features"),
        t("common-duration-participation"),
        `${t("common-onboarding")} <b>${renderNum(250)} CHF</b>`,
      ],
      href: "/static/img/Membership/WDIA_Application_Form_Observer copy.pdf",
    },
    {
      title: t("participants-3-title"),
      href: `${routesList.JOIN_US}#contact`,
    },
  ].map((item, i) => ({ ...item, id: i }));
  /* ==================================== END PARTICIPANTS DATA ========================= */

  return (
    <main className="c-main">
      <Breadcrumbs parentsData={breadcrumbsParentsData} active={t("title")} />
      <Members data={tarifsData} t={t} />
      <Participants data={participantsData} t={t} />
    </main>
  );
};
