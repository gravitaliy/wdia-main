import React from "react";
import ReactHtmlParser from "react-html-parser";
import clsx from "clsx";
import { ButtonMore } from "../../../../components/Buttons/ButtonMore/ButtonMore";
import styles from "./TarifCard.module.scss";

export const TarifCard = ({ data, darkTheme, t }) => {
  return (
    <div className={clsx(styles["tarifs-items"], darkTheme && styles.dark)}>
      {data.map((item) => (
        <div className={styles["tarifs-items-item"]} key={item.id}>
          <div className={styles["tarifs-items-item__top"]}>
            <div className={styles["tarifs-items-item__header"]}>
              <div
                className={clsx(
                  styles["tarifs-items-item__header__title"],
                  "c-text-gradient",
                )}
              >
                {item.title}
              </div>
              {item.price && (
                <div className={styles["tarifs-items-item__header__price"]}>
                  <span
                    className={styles["tarifs-items-item__header__price__big"]}
                  >
                    {item.price}
                  </span>
                  <span>CHF</span>
                </div>
              )}
              {item.fee && (
                <div className={styles["tarifs-items-item__header__fee"]}>
                  {item.fee}
                </div>
              )}
            </div>
            {item.features && (
              <div className={styles["tarifs-items-item__list"]}>
                <ul className="default-style">
                  {item.features.map((feature, i) => (
                    <li
                      className={styles["tarifs-items-item__list-item"]}
                      key={i}
                    >
                      <i
                        className={styles["tarifs-items-item__list-item__icon"]}
                      >
                        ✓
                      </i>
                      {ReactHtmlParser(feature)}
                    </li>
                  ))}
                </ul>
              </div>
            )}
          </div>
          <div className={styles["tarifs-items-item__bottom"]}>
            <ButtonMore
              href={item.href}
              target="_blank"
              rel="noreferrer noopener"
              color="blue"
              hover={darkTheme && "white"}
              title={t("join-btn")}
            />
          </div>
        </div>
      ))}
    </div>
  );
};
