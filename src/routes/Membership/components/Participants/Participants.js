import React from "react";
import clsx from "clsx";
import { TarifCard } from "../TarifCard/TarifCard";
import styles from "./Participants.module.scss";

export const Participants = ({ data, t }) => {
  return (
    <section className={clsx(styles.participants, "c-section-offset")}>
      <div className="c-container">
        <div className="c-title">{t("title-participants")}</div>
        <TarifCard data={data} darkTheme t={t} />
      </div>
    </section>
  );
};
