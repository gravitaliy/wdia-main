import React from "react";
import clsx from "clsx";
import { TarifCard } from "../TarifCard/TarifCard";
import styles from "./Members.module.scss";

export const Members = ({ data, t }) => {
  return (
    <section className={clsx(styles.members, "c-section-offset")}>
      <div className="c-container">
        <div className="c-title c-text-gradient">{t("title-members")}</div>
        <TarifCard data={data} t={t} />
      </div>
    </section>
  );
};
