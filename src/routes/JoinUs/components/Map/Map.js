import React from "react";
import clsx from "clsx";
import { MapInner } from "../../../../components/MapInner/MapInner";
import styles from "./Map.module.scss";

export const Map = ({ t }) => {
  const mapInfoData = [
    {
      country: t("common:country-2"),
      address: t("common:country-2-address"),
      phone: ["+41 225 0 87 8 87"],
      email: "contact@wdia.org",
    },
    {
      country: t("common:country-4"),
      address: t("common:country-4-address"),
      phone: ["+90 533 301 99 59", "+90 212 924 7157", "+41 75 425 14 64"],
      email: "istanbul@wdia.org",
    },
    {
      country: t("common:country-3"),
      address: t("common:country-3-address"),
      phone: ["+372 880 4247"],
      email: "contact@wdia.org",
    },
  ].map((item, i) => ({ ...item, id: i }));

  return (
    <section className={clsx(styles.map, "c-section-offset")}>
      <div className="c-container">
        <div>
          <MapInner />
          <div className={styles.map__info}>
            {mapInfoData[0].phone.forEach((phone, indx) => (
              <span>{phone[indx]}</span>
            ))}
            {mapInfoData.map((item) => (
              <div className={styles["map__info-item"]} key={item.id}>
                <div
                  className={clsx(
                    styles["map__info-item__country"],
                    "c-text-gradient",
                  )}
                >
                  {item.country}
                </div>
                <div className={styles["map__info-item-text"]}>
                  <p>
                    <span
                      className={clsx(
                        styles["map__info-item__head"],
                        "c-text-gradient",
                      )}
                    >
                      {t("map-headquarter-label")}:{" "}
                    </span>
                    <span>{item.address}</span>
                  </p>
                  <p>
                    <span
                      className={clsx(
                        styles["map__info-item__head"],
                        "c-text-gradient",
                      )}
                    >
                      {t("map-phone-label")}:{" "}
                    </span>
                    {item.phone.map((phone, idx) => (
                      <a
                        className={styles["map__info-item__tel"]}
                        href={`tel:${phone}`}
                        key={phone[idx]}
                      >
                        {phone}
                      </a>
                    ))}
                  </p>
                  <p>
                    <span
                      className={clsx(
                        styles["map__info-item__head"],
                        "c-text-gradient",
                      )}
                    >
                      {t("map-mail-label")}:{" "}
                    </span>
                    <a
                      className={styles["map__info-item__email"]}
                      href={`mailto:${item.email}`}
                    >
                      {item.email}
                    </a>
                  </p>
                </div>
              </div>
            ))}
          </div>
        </div>
      </div>
    </section>
  );
};
