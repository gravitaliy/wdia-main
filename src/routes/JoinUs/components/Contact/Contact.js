import React, { useState } from "react";
import axios from "axios";
import clsx from "clsx";
import { ToastContainer, toast } from "react-toastify";
import { Input } from "../../../../components/Inputs/Input/Input";
import { ButtonSubmit } from "../../../../components/Buttons/ButtonSubmit/ButtonSubmit";
import styles from "./Contact.module.scss";

export const Contact = ({ t }) => {
  const [state, setState] = useState({ source: 1 });

  const handleChange = (e) => {
    const { name, value } = e.target;
    setState((prev) => ({ ...prev, [name]: value }));
  };

  const onSubmit = (e) => {
    e.preventDefault();
    axios
      .post("v1/question/add", state)
      .then(({ data }) => {
        toast.success(data.data.result, {
          position: "bottom-center",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      })
      .catch((err) =>
        toast.error(err.response.data.data.message, {
          position: "bottom-center",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        }),
      );
  };

  return (
    <section className={clsx(styles.contact, "c-section-offset")} id="contact">
      <div className="c-container">
        <div className={styles["contact-inner"]}>
          <div className="c-title">{t("contact-title")}</div>
          <div>
            <form className={styles.contact__form} onSubmit={onSubmit}>
              <div className={styles["contact__form-top"]}>
                <div className={styles.contact__form__left}>
                  <Input
                    label={t("input-name-label")}
                    id="first-name"
                    name="firstname"
                    placeholder={t("input-name-placeholder")}
                    onChange={handleChange}
                    required
                  />
                  <Input
                    label={t("input-lastname-label")}
                    id="last-name"
                    name="lastname"
                    placeholder={t("input-lastname-placeholder")}
                    onChange={handleChange}
                    required
                  />
                  <Input
                    label={t("input-company-name-label")}
                    id="company-name"
                    name="companyName"
                    onChange={handleChange}
                    placeholder="WDIA"
                  />
                  <Input
                    label="E-MAIL"
                    id="email"
                    name="email"
                    type="email"
                    placeholder="alexander.ivanov@gmail.com"
                    onChange={handleChange}
                    required
                  />
                  <Input
                    label={t("input-phone-label")}
                    id="phone"
                    type="tel"
                    name="phone"
                    placeholder="+90 533 301 99 59"
                    mask="+9999999999999"
                    onChange={handleChange}
                    required
                  />
                </div>
                <div className={styles.contact__form__right}>
                  <Input
                    label={t("input-subject-label")}
                    id="subject"
                    name="subject"
                    placeholder={t("input-subject-placeholder")}
                    onChange={handleChange}
                  />
                  <Input
                    label={t("input-message-label")}
                    id="message"
                    name="message"
                    type="textarea"
                    classNameLabel={styles["contact__form-textarea-wrap"]}
                    placeholder={t("input-message-placeholder")}
                    onChange={handleChange}
                    required
                  />
                </div>
              </div>
              <div className={styles["contact__form-bottom"]}>
                <div className={styles.contact__form__left}>
                  <div className={styles.contact__form__info}>
                    *{t("required")}
                  </div>
                </div>
                <div className={styles.contact__form__right}>
                  <ButtonSubmit value={t("input-submit")} />
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
      <ToastContainer />
    </section>
  );
};
