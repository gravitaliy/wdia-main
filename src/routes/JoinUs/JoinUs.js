import React from "react";
import { useTranslation } from "react-i18next";
import { Breadcrumbs } from "../../components/Breadcrumbs/Breadcrumbs";
import { Map } from "./components/Map/Map";
import { Contact } from "./components/Contact/Contact";

const breadcrumbsParentsData = [
  { label: "WDIA", href: "/" },
].map((item, i) => ({ ...item, id: i }));

export const JoinUs = () => {
  const { t } = useTranslation(["join-us", "common"]);

  return (
    <main className="c-main">
      <Breadcrumbs
        parentsData={breadcrumbsParentsData}
        active={t("common:nav-3")}
      />
      <Map t={t} />
      <Contact t={t} />
    </main>
  );
};
