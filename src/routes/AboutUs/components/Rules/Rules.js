import React from "react";
import clsx from "clsx";
import styles from "./Rules.module.scss";

export const Rules = ({ t }) => {
  const rulesData = [
    {
      img: "/static/img/AboutUs/item-1.jpg",
      title: t("rules-item-1-title"),
      description: t("rules-item-1-text"),
    },
    {
      img: "/static/img/AboutUs/item-2.jpg",
      title: t("rules-item-2-title"),
      description: t("rules-item-2-text"),
    },
    {
      img: "/static/img/AboutUs/item-3.jpg",
      title: t("rules-item-3-title"),
      description: t("rules-item-3-text"),
    },
  ].map((item, i) => ({ ...item, id: i }));

  return (
    <section className="c-section-offset">
      <div className="c-container">
        <div className="c-title c-text-gradient">{t("rules-title")}</div>
        <div className={styles.rules__items}>
          {rulesData.map((item) => (
            <div className={styles["rules__items-item"]} key={item.id}>
              <div className={styles["rules__items-item__img-wrap"]}>
                <img className="fix-img img-fit" src={item.img} alt="" />
              </div>
              <div className={styles["rules__items-item__text"]}>
                <div
                  className={clsx(
                    styles["rules__items-item__text__title"],
                    "c-text-gradient",
                  )}
                >
                  {item.title}
                </div>
                <div className={styles["rules__items-item__text__description"]}>
                  {item.description}
                </div>
              </div>
            </div>
          ))}
        </div>
      </div>
    </section>
  );
};
