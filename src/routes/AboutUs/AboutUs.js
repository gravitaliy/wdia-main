import React from "react";
import { useTranslation } from "react-i18next";
import { Breadcrumbs } from "../../components/Breadcrumbs/Breadcrumbs";
import { PromoPage } from "../../components/PromoPage/PromoPage";
import { Rules } from "./components/Rules/Rules";

const breadcrumbsParentsData = [
  { label: "WDIA", href: "/" },
].map((item, i) => ({ ...item, id: i }));

export const AboutUs = () => {
  const { t } = useTranslation(["about-us", "common"]);

  return (
    <main className="c-main">
      <Breadcrumbs
        parentsData={breadcrumbsParentsData}
        active={t("common:nav-1-2")}
      />
      <PromoPage title={t("promo-title")}>
        <p>{t("promo-text-1")}</p>
        <p>{t("promo-text-2")}</p>
      </PromoPage>
      <Rules t={t} />
    </main>
  );
};
