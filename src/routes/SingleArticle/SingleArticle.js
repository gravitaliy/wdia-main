import React from "react";
import { useTranslation } from "react-i18next";
import ReactHtmlParser from "react-html-parser";
import { useRouter } from "next/router";
import clsx from "clsx";
import { dataEvents } from "../../assets/enums/dataEvents";
import { Breadcrumbs } from "../../components/Breadcrumbs/Breadcrumbs";
import { routesList } from "../../assets/enums/routesList";
import styles from "./SingleArticle.module.scss";

export const SingleArticle = () => {
  const { t } = useTranslation("common");

  const router = useRouter();
  const { id } = router.query;
  const article = dataEvents()[id];

  const breadcrumbsParentsData = [
    { label: "WDIA", href: "/" },
    { label: t("nav-2"), href: `${routesList.OUR_ACTIVITIES}#events` },
  ].map((item, i) => ({ ...item, id: i }));

  return (
    <main className="c-main">
      <Breadcrumbs
        parentsData={breadcrumbsParentsData}
        active={article.title}
      />
      <section className={clsx(styles.article, "c-section-offset")}>
        <div className="c-container">
          <img
            className={clsx(styles["article-img-head"], "fix-img")}
            src={article.imgLg}
            alt=""
          />
          <div className={styles["article-inner"]}>
            {ReactHtmlParser(article.contentStr)}
          </div>
        </div>
      </section>
    </main>
  );
};
