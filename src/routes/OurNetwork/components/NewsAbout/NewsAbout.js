import React from "react";
import clsx from "clsx";
import styles from "./NewsAbout.module.scss";

const newsAboutData = [
  {
    img: "/static/img/OurNetwork/logo/item-1.jpg",
    title: "Businesstanbul",
    subtitle: "Representative: Ahmet çagri Karamisir PhD.",
    desctiption:
      "Businesstanbul.com is designed to serve as your guiding light along the path of conducting successful business operations in Istanbul   and in all GCC Countries namely Kingdom of Saudi Arabia, Kuwait, UAE, Qatar, Bahrain and Oman. Our Administrative and Associate teams will provide you with the knowledge and tools that will enable you to make informed decisions that will efficiently lead your firm to the achievement of your strategic goals and objectives. You will experience peace of mind knowing that you have greatly reduced the risk of doing business in a foreign environment by choosing a team of professionals with the know-how to fulfill your informational and service needs as well as guide you through the unique and sometimes challenging business world in  Istanbul , the commercial capital of  Turkey as well as in the GCC Countries.",
  },
  {
    img: "/static/img/OurNetwork/logo/item-1.jpg",
    title: "Businesstanbul",
    subtitle: "Representative: Ahmet çagri Karamisir PhD.",
    desctiption:
      "Businesstanbul.com is designed to serve as your guiding light along the path of conducting successful business operations in Istanbul   and in all GCC Countries namely Kingdom of Saudi Arabia, Kuwait, UAE, Qatar, Bahrain and Oman. Our Administrative and Associate teams will provide you with the knowledge and tools that will enable you to make informed decisions that will efficiently lead your firm to the achievement of your strategic goals and objectives. You will experience peace of mind knowing that you have greatly reduced the risk of doing business in a foreign environment by choosing a team of professionals with the know-how to fulfill your informational and service needs as well as guide you through the unique and sometimes challenging business world in  Istanbul , the commercial capital of  Turkey as well as in the GCC Countries.",
  },
  {
    img: "/static/img/OurNetwork/logo/item-1.jpg",
    title: "Businesstanbul",
    subtitle: "Representative: Ahmet çagri Karamisir PhD.",
    desctiption:
      "Businesstanbul.com is designed to serve as your guiding light along the path of conducting successful business operations in Istanbul   and in all GCC Countries namely Kingdom of Saudi Arabia, Kuwait, UAE, Qatar, Bahrain and Oman. Our Administrative and Associate teams will provide you with the knowledge and tools that will enable you to make informed decisions that will efficiently lead your firm to the achievement of your strategic goals and objectives. You will experience peace of mind knowing that you have greatly reduced the risk of doing business in a foreign environment by choosing a team of professionals with the know-how to fulfill your informational and service needs as well as guide you through the unique and sometimes challenging business world in  Istanbul , the commercial capital of  Turkey as well as in the GCC Countries.",
  },
  {
    img: "/static/img/OurNetwork/logo/item-1.jpg",
    title: "Businesstanbul",
    subtitle: "Representative: Ahmet çagri Karamisir PhD.",
    desctiption:
      "Businesstanbul.com is designed to serve as your guiding light along the path of conducting successful business operations in Istanbul   and in all GCC Countries namely Kingdom of Saudi Arabia, Kuwait, UAE, Qatar, Bahrain and Oman. Our Administrative and Associate teams will provide you with the knowledge and tools that will enable you to make informed decisions that will efficiently lead your firm to the achievement of your strategic goals and objectives. You will experience peace of mind knowing that you have greatly reduced the risk of doing business in a foreign environment by choosing a team of professionals with the know-how to fulfill your informational and service needs as well as guide you through the unique and sometimes challenging business world in  Istanbul , the commercial capital of  Turkey as well as in the GCC Countries.",
  },
].map((item, i) => ({ ...item, id: i }));

export const NewsAbout = () => {
  return (
    <section className={clsx(styles["news-about"], "c-section-offset")}>
      <div className="c-container">
        <div className={styles["news-about-inner"]}>
          <div className={styles["news-about__items"]}>
            {newsAboutData.map((item) => (
              <div className={styles["news-about__items-item"]} key={item.id}>
                <div className={styles["news-about__items-item__img-wrap"]}>
                  <img className="fix-img" src={item.img} alt="" />
                </div>
                <div
                  className={clsx(
                    styles["news-about__items-item__title"],
                    "c-text-gradient",
                  )}
                >
                  {item.title}
                </div>
                <div className={styles["news-about__items-item__subtitle"]}>
                  {item.subtitle}
                </div>
                <div className={styles["news-about__items-item__desctiption"]}>
                  {item.desctiption}
                </div>
              </div>
            ))}
          </div>
        </div>
      </div>
    </section>
  );
};
