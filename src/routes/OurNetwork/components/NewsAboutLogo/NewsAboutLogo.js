import React from "react";
import styles from "./NewsAboutLogo.module.scss";

const logoData = [
  { img: "/static/img/OurNetwork/logo/item-1.jpg" },
  { img: "/static/img/OurNetwork/logo/item-2.jpg" },
  { img: "/static/img/OurNetwork/logo/item-3.jpg" },
  { img: "/static/img/OurNetwork/logo/item-4.jpg" },
].map((item, i) => ({ ...item, id: i }));

export const NewsAboutLogo = () => {
  return (
    <section className="c-section-offset">
      <div className="c-container">
        <div className={styles["news-about-logo__items"]}>
          {logoData.map((item) => (
            <div
              className={styles["news-about-logo__items-item"]}
              key={item.id}
            >
              <img className="fix-img" src={item.img} alt="" />
            </div>
          ))}
        </div>
      </div>
    </section>
  );
};
