import React from "react";
import { useTranslation } from "react-i18next";
import { Breadcrumbs } from "../../components/Breadcrumbs/Breadcrumbs";
import { PromoPage } from "../../components/PromoPage/PromoPage";
import { NewsAboutLogo } from "./components/NewsAboutLogo/NewsAboutLogo";
// import { NewsAbout } from "./components/NewsAbout/NewsAbout";

const breadcrumbsParentsData = [
  { label: "WDIA", href: "/" },
].map((item, i) => ({ ...item, id: i }));

export const OurNetwork = () => {
  const { t } = useTranslation(["our-network", "common"]);

  return (
    <main className="c-main">
      <Breadcrumbs
        parentsData={breadcrumbsParentsData}
        active={t("common:nav-5")}
      />
      <PromoPage title={t("promo-title")}>
        <p>{t("promo-text")}</p>
      </PromoPage>
      <NewsAboutLogo />
      {/* <NewsAbout /> */}
    </main>
  );
};
