import React from "react";
import clsx from "clsx";
import { useTranslation } from "react-i18next";
import Link from "next/link";
import { Title } from "../../components/Typography/Title/Title";
import { linksMenu, linksAboutUs, linksIcons } from "./links";
import styles from "./Footer.module.scss";

export const Footer = () => {
  const { t } = useTranslation(["footer", "common"]);

  return (
    <footer className={styles.footer}>
      <div className="c-container">
        <div className={styles["footer-list"]}>
          <div
            className={clsx(styles["footer-item"], styles["footer-info"])}
            style={{ paddingTop: 30 }}
          >
            <Link href="/">
              <a href="/">
                <img
                  className={styles["footer-logo"]}
                  src="/static/img/Footer/logo-white.svg"
                  alt="logo"
                />
              </a>
            </Link>
            <p className={styles.copyright}>© 2020 WDIA</p>
          </div>
          <div className={styles["footer-links"]}>
            <div className={styles["footer-item"]}>
              <Title className={styles["footer-item-title"]}>
                {t("title-1")}
              </Title>
              <ul className={styles["footer-list-links"]}>
                {linksAboutUs().map((item) => (
                  <li key={item.id}>
                    <Link href={item.href}>
                      <a href={item.href}>{item.label}</a>
                    </Link>
                  </li>
                ))}
              </ul>
            </div>
            <div className={styles["footer-item"]}>
              <Title className={styles["footer-item-title"]}>
                {t("title-2")}
              </Title>
              <ul className={styles["footer-list-links"]}>
                {linksMenu().map((item) => (
                  <li key={item.id}>
                    <Link href={item.link}>
                      <a
                        target={item.blank ? "_blank" : ""}
                        rel="noopener noreferrer"
                        href={item.link}
                      >
                        {item.label}
                      </a>
                    </Link>
                  </li>
                ))}
              </ul>
            </div>
          </div>
          <div
            className={clsx(styles["footer-item"], styles["footer-socials"])}
          >
            <div>
              <Title className={styles["footer-item-title"]}>
                {t("title-3")}
              </Title>
              <div className={styles["footer-list-icons"]}>
                {linksIcons.map((item) => (
                  <a
                    href={item.link}
                    target="_blank"
                    rel="noopener noreferrer"
                    key={item.id}
                  >
                    <img src={item.icon} alt="icon" />
                  </a>
                ))}
              </div>
            </div>
          </div>
        </div>
      </div>
    </footer>
  );
};
