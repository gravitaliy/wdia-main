import { useTranslation } from "react-i18next";
import { routesList } from "../../assets/enums/routesList";

/* ====================== LINKS ICONS ======================== */

export const linksIcons = [
  {
    icon: "/static/img/Footer/icons/1.svg",
    link: "mailto:contact@wdia.org",
  },
  {
    icon: "/static/img/Footer/icons/2.svg",
    link: "https://twitter.com/wdia_org",
  },
  {
    icon: "/static/img/Footer/icons/3.svg",
    link: "https://t.me/wdia_org",
  },
  {
    icon: "/static/img/Footer/icons/5.svg",
    link:
      "https://www.meetup.com/ru-RU/The-Worldwide-Digital-Identification-Association-WDIA/members/298332190/",
  },
  {
    icon: "/static/img/Footer/icons/6.svg",
    link: "https://www.instagram.com/wdiaorg/",
  },
  {
    icon: "/static/img/Footer/icons/7.svg",
    link: "https://medium.com/@wdia.org",
  },
  {
    icon: "/static/img/Footer/icons/8.svg",
    link: "https://www.facebook.com/wdia.org",
  },
  {
    icon: "/static/img/Footer/icons/9.svg",
    link: "https://www.linkedin.com/company/wdia/",
  },
].map((item, i) => ({ ...item, id: i }));

/* ====================== LINKS MENU ======================== */

export const linksMenu = () => {
  const { t } = useTranslation("footer");

  return [
    {
      label: t("common:nav-2"),
      link: routesList.OUR_ACTIVITIES,
      blank: false,
    },
    {
      label: t("menu-2"),
      link: "https://dev-admin.wdia.org/",
      blank: true,
    },
    {
      label: t("menu-3"),
      link: "",
      blank: false,
    },
    {
      label: t("menu-4"),
      link: "/static/img/Footer/WDIA_Privacy_Policy.pdf",
      blank: true,
    },
    {
      label: t("menu-5"),
      link: "/static/img/Footer/WDIA_Terms_Use.pdf",
      blank: true,
    },
  ].map((item, i) => ({ ...item, id: i }));
};

/* ====================== LINKS ABOUT US ======================== */

export const linksAboutUs = () => {
  const { t } = useTranslation("footer");

  return [
    {
      label: t("common:nav-1-1"),
      href: routesList.ABOUT_TECH,
    },
    {
      label: t("common:nav-1-2"),
      href: routesList.ABOUT_US,
    },
    {
      label: t("common:nav-4"),
      href: routesList.MEMBERSHIP,
    },
    {
      label: t("common:nav-5"),
      href: routesList.OUR_NETWORK,
    },
    {
      label: t("common:nav-3"),
      href: routesList.CONTACTS,
    },
  ].map((item, i) => ({ ...item, id: i }));
};
