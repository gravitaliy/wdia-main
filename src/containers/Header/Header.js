import React, { useState } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import { useTranslation } from "react-i18next";
import clsx from "clsx";
import Dropdown from "rc-dropdown";
import { routesList } from "../../assets/enums/routesList";
import { Button } from "../../components/Buttons/Button/Button";
import { Lang } from "./components/Lang/Lang";
import styles from "./Header.module.scss";
// import { SearchInput } from "../../components/Inputs/SearchInput/SearchInput";

/*
const listIcons = [
  {
    href: "https://www.instagram.com/wdiaorg/",
    img: "/static/img/Header/header_icons/6.svg",
  },
  {
    href: "mailto:contact@wdia.org",
    img: "/static/img/Header/header_icons/1.svg",
  },
  {
    href: "https://twitter.com/wdia_org",
    img: "/static/img/Header/header_icons/4.svg",
  },
  {
    href: "https://t.me/wdia_org",
    img: "/static/img/Header/header_icons/7.svg",
  },
].map((item, i) => ({ ...item, id: i }));
*/

export const Header = () => {
  const [isOpen, setIsOpen] = useState(false);

  const router = useRouter();

  const { t } = useTranslation("common");

  const navbar = [
    {
      label: t("nav-1"),
      child: [
        {
          label: t("nav-1-1"),
          link: routesList.ABOUT_TECH,
        },
        {
          label: t("nav-1-2"),
          link: routesList.ABOUT_US,
        },
      ],
    },
    {
      label: t("nav-2"),
      link: routesList.OUR_ACTIVITIES,
    },
    {
      label: t("nav-3"),
      link: routesList.JOIN_US,
    },
  ].map((item, i) => ({ ...item, id: i }));

  const onClickNavItem = (e) => {
    if (isOpen && e.target.id !== "js-navbar-item-with-child") {
      toggleOpenMenu();
    }
  };

  const toggleOpenMenu = () => {
    setIsOpen(!isOpen);
  };

  return (
    <header className={styles["navbar-wrap"]}>
      <div className="c-container">
        <div
          className={clsx(
            styles.navbar,
            "flex",
            "justify-between",
            "align-center",
          )}
        >
          <Link href="/">
            <a className={styles["navbar-icon-wrap"]} href="/">
              <img
                className="fix-img"
                src="/static/img/Header/logo.svg"
                alt="logo"
              />
            </a>
          </Link>
          {/*
          <div className={styles["block-search"]}>
            <SearchInput />
          </div>
          */}
          {/* eslint-disable-next-line */}
          <div
            onClick={(e) => onClickNavItem(e)}
            className={clsx(styles["list-navbar-links"], isOpen && styles.open)}
          >
            <div className={styles["list-links"]}>
              {navbar.map((item) => {
                const menu = () => (
                  <div className={styles["navbar-child-items"]} key={item.id}>
                    {item.child.map((child, i) => (
                      <div
                        className={styles["navbar-child-items-item"]}
                        key={i}
                      >
                        <Link href={child.link}>
                          <a
                            className={clsx(
                              styles["navbar-item"],
                              styles["navbar-link"],
                              router.pathname === child.link && styles.active,
                            )}
                            href={child.link}
                          >
                            {child.label}
                          </a>
                        </Link>
                      </div>
                    ))}
                  </div>
                );

                return item.child ? (
                  <Dropdown
                    overlay={menu}
                    trigger={["click"]}
                    animation="slide-up"
                    key={item.id}
                  >
                    <span
                      className={clsx(
                        styles["navbar-item"],
                        styles["navbar-item-with-child"],
                      )}
                      id="js-navbar-item-with-child"
                    >
                      {item.label}
                    </span>
                  </Dropdown>
                ) : (
                  <Link href={item.link} key={item.id}>
                    <a
                      className={clsx(
                        styles["navbar-link"],
                        styles["navbar-item"],
                        router.pathname === item.link && styles.active,
                      )}
                      href={item.link}
                    >
                      {item.label}
                    </a>
                  </Link>
                );
              })}
            </div>
          </div>

          <div className={styles["lang-wrap"]}>
            <Lang />
          </div>

          <div className={styles["login-button"]}>
            <a
              style={{ textDecoration: "none", color: "white" }}
              href="https://dev-admin.wdia.org/"
              target="_blank"
              rel="noopener noreferrer"
            >
              <Button>{t("btn-login")}</Button>
            </a>
          </div>

          {/*
          <ul className={styles["header-list-icons"]}>
            {listIcons.map(item => (
              <li key={item.id}>
                <a
                  href={item.href}
                  target="_blank"
                  rel="noreferrer noopener"
                >
                  <img src={item.img} alt="icon" />
                </a>
              </li>
            ))}
          </ul>
          */}
          <button
            onClick={toggleOpenMenu}
            type="button"
            className={styles["burger-menu-button"]}
          >
            <img src="/static/img/_general/burger.svg" alt="burger" />
          </button>
        </div>
      </div>
    </header>
  );
};
