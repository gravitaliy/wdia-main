import React, { memo } from "react";
import { useTranslation } from "react-i18next";
import Select, { components } from "react-select";

import styles from "./Lang.module.scss";

const langData = [
  {
    value: "en",
    label: "English",
    icon: "/static/img/Header/Lang/en.svg",
  },
  {
    value: "ru",
    label: "Русский",
    icon: "/static/img/Header/Lang/ru.svg",
  },
];

const IconOption = (props) => {
  return (
    // eslint-disable-next-line react/jsx-props-no-spreading
    <components.Option {...props}>
      <div className={styles["lang-single-value"]}>
        <img className={styles["lang-icon"]} src={props.data.icon} alt="" />
        {props.data.label}
      </div>
    </components.Option>
  );
};

const IconSingleValue = ({ children, ...props }) => {
  return (
    // eslint-disable-next-line react/jsx-props-no-spreading
    <components.SingleValue {...props}>
      <div className={styles["lang-single-value"]}>
        <img className={styles["lang-icon"]} src={props.data.icon} alt="" />
        {children}
      </div>
    </components.SingleValue>
  );
};

export const Lang = memo(() => {
  const { i18n } = useTranslation();

  const handleChange = (selectedOption) => {
    i18n.changeLanguage(selectedOption.value);
  };

  const customStyles = {
    container: (provider) => ({
      ...provider,
      display: "inline-block",
      width: 135,
    }),
    control: () => ({
      display: "flex",
      flexWrap: "wrap",
      alignItems: "center",
      justifyContent: "space-between",
      cursor: "pointer",
    }),
    dropdownIndicator: (provider) => ({
      ...provider,
      paddingTop: 0,
      paddingBottom: 0,
      paddingRight: 0,
    }),
  };

  const selectedValue = () => {
    const isHasLang = langData.some(item => item.value === i18n.language);
    return langData.filter(
      (option) => {
        if (isHasLang) return option.value === i18n.language;
        return option.value === "en";
      },
    )
  };

  return (
    <Select
      instanceId="lang"
      value={selectedValue()}
      onChange={handleChange}
      options={langData}
      isSearchable={false}
      components={{
        Option: IconOption,
        SingleValue: IconSingleValue,
        IndicatorSeparator: () => null,
      }}
      styles={customStyles}
    />
  );
});
