import React from "react";
import Head from "next/head";
import { Header } from "./containers/Header/Header";
import { Footer } from "./containers/Footer/Footer";

export const App = ({ children }) => {
  return (
    <>
      <Head>
        <meta charSet="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="description" content="WDIA" />
        <meta name="theme-color" content="#21BA45" />
        <meta name="apple-mobile-web-app-status-bar-style" content="#21BA45" />
        <link
          rel="icon"
          type="image/png"
          sizes="32x32"
          href="/static/img/favicon/favicon-32x32.png"
        />
        <link
          rel="apple-touch-icon"
          sizes="180x180"
          href="/static/img/favicon/apple-touch-icon.png"
        />
        <link
          rel="icon"
          type="image/png"
          sizes="16x16"
          href="/static/img/favicon/favicon-16x16.png"
        />
        <link
          href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;700&display=swap"
          rel="stylesheet"
        />
        <title>WDIA – Worldwide Digital Identification Association</title>
      </Head>
      <Header />
      <main>{children}</main>
      <Footer />
    </>
  );
};
