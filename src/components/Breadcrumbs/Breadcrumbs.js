import React from "react";
import Link from "next/link";
import clsx from "clsx";
import styles from "./Breadcrumbs.module.scss";

export const Breadcrumbs = ({ parentsData, active }) => {
  return (
    <div className={styles.breadcrumbs}>
      <div className="c-container">
        <div className={styles["breadcrumbs-inner"]}>
          <ul className={clsx(styles.breadcrumbs__content, "default-style")}>
            {parentsData.map((item) => (
              <li
                className={clsx(
                  styles["breadcrumbs__content-item"],
                  "c-text-gradient",
                )}
                key={item.id}
              >
                <Link href={item.href}>
                  <a
                    className={styles["breadcrumbs__content-item-link"]}
                    href={item.href}
                  >
                    {item.label}
                  </a>
                </Link>
              </li>
            ))}
            <li className={styles["breadcrumbs__content-item"]}>{active}</li>
          </ul>
          <div className={clsx(styles.breadcrumbs__title, "c-text-gradient")}>
            {active}
          </div>
        </div>
      </div>
    </div>
  );
};
