import React from "react";
import clsx from "clsx";
import styles from "./Title.module.scss";

export const Title = ({ children, className, style, color }) => (
  <h3 className={clsx(styles.title, className, styles[color])} style={style}>
    {children}
  </h3>
);
