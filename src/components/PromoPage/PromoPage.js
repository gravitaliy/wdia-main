import React from "react";
import clsx from "clsx";
import styles from "./PromoPage.module.scss";

export const PromoPage = ({ title, children }) => {
  return (
    <section className={clsx(styles["promo-page"], "c-section-offset")}>
      <div className="c-container">
        <div className={styles["promo-page-inner"]}>
          <h3 className={styles["promo-page__title"]}>{title}</h3>
          <div className={styles["promo-page__description"]}>{children}</div>
        </div>
      </div>
    </section>
  );
};
