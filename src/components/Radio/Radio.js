import React from "react";
import styles from "./Radio.module.scss";

/* eslint-disable jsx-a11y/label-has-associated-control */
export const Radio = ({ name, value, checked, id, handleChange }) => (
  <div className={styles["radio-container"]}>
    <input
      id={id}
      type="radio"
      name={name}
      value={value}
      checked={checked}
      onChange={handleChange}
      className={styles["radio-button"]}
    />
    <label htmlFor={id} />
  </div>
);
