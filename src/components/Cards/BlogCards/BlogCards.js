import React from "react";
import { useTranslation } from "react-i18next";
import clsx from "clsx";
import styles from "./BlogCards.module.scss";
import { getDate, renderDate } from "../../../assets/enums/moment";

export const BlogCards = ({ data, sort }) => {
  const { t } = useTranslation("blog");

  const newData = data.slice(0);

  const sortData = newData.sort((a, b) => {
    if (sort === "asc") {
      return getDate(a.date) > getDate(b.date) ? 1 : -1;
    }

    if (sort === "desc") {
      return getDate(a.date) < getDate(b.date) ? 1 : -1;
    }

    return newData;
  });

  return (
    <div className={styles["blog-items"]}>
      {sortData.map((item) => (
        <div className={styles["blog-items-item"]} key={item.id}>
          <div className={styles["blog-items-item__img-wrap"]}>
            <img className="fix-img img-fit" src={item.img} alt="" />
          </div>
          <div className={styles["blog-items-item__text"]}>
            <div className={styles["blog-items-item__text__date"]}>
              {renderDate(item.date, "DD MMMM YYYY")}
            </div>
            <div
              className={clsx(
                styles["blog-items-item__text__title"],
                "c-text-gradient",
              )}
            >
              {item.title}
            </div>
            <div className={styles["blog-items-item__text__description"]}>
              {item.description}
            </div>
            <a
              className={clsx(
                styles["blog-items-item__text__more"],
                "c-text-gradient",
              )}
              href={item.moreLink}
              target="_blank"
              rel="noopener noreferrer"
            >
              {t("read-more-btn")}...
            </a>
          </div>
        </div>
      ))}
    </div>
  );
};
