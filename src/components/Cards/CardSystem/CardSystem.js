import React from "react";
import styles from "./CardSystem.module.scss";

export const CardSystem = ({ t }) => {
  const ecosystemData = [
    {
      img: "/static/img/Main/ecosystem/1.svg",
      title: t("ecosystem-item-1-title"),
      list: [
        t("ecosystem-item-1-list-1"),
        t("ecosystem-item-1-list-2"),
        t("ecosystem-item-1-list-3"),
      ],
    },
    {
      img: "/static/img/Main/ecosystem/benchmark 1.svg",
      title: t("ecosystem-item-2-title"),
      list: [
        t("ecosystem-item-2-list-1"),
        t("ecosystem-item-2-list-2"),
        t("ecosystem-item-2-list-3"),
      ],
    },
    {
      img: "/static/img/Main/ecosystem/2.svg",
      title: t("ecosystem-item-3-title"),
      list: [t("ecosystem-item-3-list-1"), t("ecosystem-item-3-list-2")],
    },
    {
      img: "/static/img/Main/ecosystem/4.svg",
      title: t("ecosystem-item-4-title"),
      list: [t("ecosystem-item-4-list-1"), t("ecosystem-item-4-list-2")],
    },
    {
      img: "/static/img/Main/ecosystem/Frame 4.svg",
      title: t("ecosystem-item-5-title"),
      list: [t("ecosystem-item-5-list-1"), t("ecosystem-item-5-list-2")],
    },
    {
      img: "/static/img/Main/ecosystem/Group 14.svg",
      title: t("ecosystem-item-6-title"),
      list: [t("ecosystem-item-6-list-1"), t("ecosystem-item-6-list-2")],
    },
    {
      img: "/static/img/Main/ecosystem/Frame 5.svg",
      title: t("ecosystem-item-7-title"),
      list: [t("ecosystem-item-7-list-1"), t("ecosystem-item-7-list-2")],
    },
    {
      img: "/static/img/Main/ecosystem/8.svg",
      title: t("ecosystem-item-8-title"),
      list: [t("ecosystem-item-8-list-1"), t("ecosystem-item-8-list-2")],
    },
    {
      img: "/static/img/Main/ecosystem/Frame 6.svg",
      title: t("ecosystem-item-9-title"),
      list: [t("ecosystem-item-9-list-1")],
    },
    {
      img: "/static/img/Main/ecosystem/truck 1.svg",
      title: t("ecosystem-item-10-title"),
      list: [t("ecosystem-item-10-list-1"), t("ecosystem-item-10-list-2")],
    },
    {
      img: "/static/img/Main/ecosystem/portal 1.svg",
      title: t("ecosystem-item-11-title"),
      list: [t("ecosystem-item-11-list-1"), t("ecosystem-item-11-list-2")],
    },
    {
      img: "/static/img/Main/ecosystem/control 1.svg",
      title: t("ecosystem-item-12-title"),
      list: [],
    },
    {
      img: "/static/img/Main/ecosystem/retail 1.svg",
      title: t("ecosystem-item-13-title"),
      list: [],
    },
  ].map((item, i) => ({ ...item, id: i }));

  return ecosystemData.map((item) => (
    <div className={styles["card-system"]} key={item.id}>
      <img src={item.img} alt="card system" />
      <h4>{item.title}</h4>
      <ul>
        {item.list.map((childItem, i) => (
          <li key={i}>
            <p>{childItem}</p>
          </li>
        ))}
      </ul>
    </div>
  ));
};
