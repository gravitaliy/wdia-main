import React, { useEffect } from "react";
import { useTranslation } from "react-i18next";
import moment from "moment";
import clsx from "clsx";
import { ButtonMore } from "../../Buttons/ButtonMore/ButtonMore";
import { getDate, renderDate } from "../../../assets/enums/moment";
import styles from "./EventsNewsCards.module.scss";

export const EventsNewsCards = ({
  data,
  type,
  sort,
  onSetCount,
  theme,
  variant,
}) => {
  const { t } = useTranslation("events");

  const sortData = () => {
    const newData = variant === "mix" ? data.slice(0, 3) : data.slice(0);

    return newData.sort((a, b) => {
      switch (sort) {
        case "asc":
          return getDate(a.date) > getDate(b.date) ? 1 : -1;
        case "desc":
          return getDate(a.date) < getDate(b.date) ? 1 : -1;
        default:
          return newData;
      }
    });
  }

  const filterData = (time) => (
    sortData().filter((item) => {
      switch (time) {
        case "future":
          return getDate(item.date) > moment();
        case "past":
          return getDate(item.date) < moment();
        default:
          return getDate(item.date);
      }
    })
  );

  const RenderEvents = ({ data }) => {
    useEffect(() => {
      if (onSetCount) {
        onSetCount(data.length);
      }
    }, []);

    return (
      <>
        {data.length !== 0 && (
          <div
            className={clsx(
              styles["event-calendar__items-title-wrap"],
              "c-sm-hide",
            )}
          >
            <div
              className={clsx(
                styles["event-calendar__items-title"],
                "c-text-gradient",
              )}
            >
              {type === "past" && t("past-label")}
              {type === "future" && t("upcoming-label")}
            </div>
          </div>
        )}
        <div className={styles["event-news__items"]}>
          {data.map((item) => (
            <div
              className={clsx(styles["event-news__items-item"], styles[theme])}
              key={item.id}
            >
              {variant === "mix" && (
                <a
                  className={styles["event-news__items-item__type"]}
                  href={item.showAll}
                >
                  {item.type}
                </a>
              )}
              <div className={styles["event-news__items-item__img-wrap"]}>
                <img
                  className="fix-img img-fit"
                  src={item.imgMd}
                  width="540"
                  alt=""
                  srcSet={`
                      ${item.imgMd} 540w,
                      ${item.imgSm} 350w,
                    `}
                  sizes="
                      (max-width: 767px) 540px,
                      350px,
                    "
                />
              </div>
              <div className={styles["event-news__items-item__text"]}>
                <div className={styles["event-news__items-item__text-title"]}>
                  {item.title}
                </div>
                <div className={styles["event-news__items-item__text-bottom"]}>
                  <div
                    className={
                      styles["event-news__items-item__text-bottom__date"]
                    }
                  >
                    {renderDate(item.date, "DD MMMM YYYY")}
                  </div>
                  <ButtonMore
                    color={theme === "dark" ? "white" : "grey"}
                    size="sm"
                    arrow
                    forDynamicLinkItem={item}
                  />
                </div>
              </div>
            </div>
          ))}
        </div>
      </>
    )
  }

  return (
    <>
      {type === "future" && (
        <RenderEvents data={filterData("future")} />
      )}
      {type === "past" && (
        <RenderEvents data={filterData("past")} />
      )}

      {!type && (
        <RenderEvents data={sortData()} />
      )}
    </>
  );
};
