import React from "react";
import { ButtonMore } from "../../Buttons/ButtonMore/ButtonMore";
import { getDate, renderDate } from "../../../assets/enums/moment";
import styles from "./InfoCards.module.scss";

export const InfoCards = ({ data, sort }) => {
  const newData = data.slice(0);

  const sortData = newData.sort((a, b) => {
    if (sort === "asc") {
      return getDate(a.date) > getDate(b.date) ? 1 : -1;
    }

    if (sort === "desc") {
      return getDate(a.date) < getDate(b.date) ? 1 : -1;
    }

    return newData;
  });

  return (
    <div className={styles["cards-info-items"]}>
      {sortData.map((item) => (
        <div className={styles["cards-info-items-item"]} key={item.id}>
          <div className={styles["cards-info-items-item__left"]}>
            <div className={styles["cards-info-items-item__left__img-wrap"]}>
              <img className="fix-img img-fit" src={item.img} alt="" />
            </div>
            <div className={styles["cards-info-items-item__left__date"]}>
              {renderDate(item.date, "MMMM DD, YYYY")}
            </div>
          </div>
          <div className={styles["cards-info-items-item__text"]}>
            <div className={styles["cards-info-items-item__text__title"]}>
              {item.title}
            </div>
            <div className={styles["cards-info-items-item__text__description"]}>
              {item.description}
            </div>
            <div className={styles["cards-info-items-item__text__btn-wrap"]}>
              <ButtonMore
                href={item.link}
                color="white"
                size="md"
                arrow
                target="_blank"
                rel="noreferrer noopener"
              />
            </div>
          </div>
        </div>
      ))}
    </div>
  );
};
