import React from "react";
import clsx from "clsx";
import { AboutCards } from "./components/AboutCards/AboutCards";
import styles from "./AboutCardsWrap.module.scss";

export const AboutCardsWrap = ({ data, title }) => {
  return (
    <section className={clsx(styles["about-cards-wrap"], "c-section-offset")}>
      <div className="c-container">
        <div>
          <div className="c-title">{title}</div>
          <AboutCards data={data} />
        </div>
      </div>
    </section>
  );
};
