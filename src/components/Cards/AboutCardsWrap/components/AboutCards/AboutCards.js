import React from "react";
import styles from "./AboutCards.module.scss";

export const AboutCards = ({ data }) => {
  return (
    <div className={styles["cards-about-items"]}>
      {data.map((item) => (
        <div className={styles["cards-about-items-item"]} key={item.id}>
          <div className={styles["cards-about-items-item__left"]}>
            <div className={styles["cards-about-items-item__left__img-wrap"]}>
              <img className="fix-img img-fit" src={item.img} alt="" />
            </div>
          </div>
          <div className={styles["cards-about-items-item__text"]}>
            <div className={styles["cards-about-items-item__text__title"]}>
              {item.title}
            </div>
            {item.description && (
              <div
                className={styles["cards-about-items-item__text__description"]}
              >
                {item.description}
              </div>
            )}
          </div>
        </div>
      ))}
    </div>
  );
};
