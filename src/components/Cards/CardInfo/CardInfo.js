import React from "react";
import { Title } from "../../Typography/Title/Title";
import styles from "./CardInfo.module.scss";

export const CardInfo = ({ t }) => {
  const aboutData = [
    {
      imgMd: "/static/img/Main/about/about1/about1-540.jpg",
      imgSm: "/static/img/Main/about/about1/about1-350.jpg",
      title: t("about-item-1-title"),
      text: t("about-item-1-text"),
    },
    {
      imgMd: "/static/img/Main/about/about2/about2-540.jpg",
      imgSm: "/static/img/Main/about/about2/about2-350.jpg",
      title: t("about-item-2-title"),
      text: t("about-item-2-text"),
    },
    {
      imgMd: "/static/img/Main/about/about3/about3-540.jpg",
      imgSm: "/static/img/Main/about/about3/about3-350.jpg",
      title: t("about-item-3-title"),
      text: t("about-item-3-text"),
    },
  ].map((item, i) => ({ ...item, id: i }));

  return aboutData.map((item) => (
    <div className={styles["card-info"]} key={item.id}>
      <div className={styles["card-info-image-wrap"]}>
        <img
          className="fix-img img-fit"
          src={item.imgMd}
          width="540"
          alt="card info"
          srcSet={`
              ${item.imgMd} 540w,
              ${item.imgSm} 350w,
            `}
          sizes="
              (max-width: 991px) 540px,
              350px,
            "
        />
      </div>
      <div className={styles["card-info-content"]}>
        <div>
          <Title style={{ color: "white", fontSize: 28 }}>{item.title}</Title>
          <p className={styles["content-text"]}>{item.text}</p>
        </div>
      </div>
    </div>
  ));
};
