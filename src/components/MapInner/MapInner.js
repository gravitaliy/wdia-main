import React from "react";
import { useTranslation } from "react-i18next";
import clsx from "clsx";
import styles from "./MapInner.module.scss";

export const MapInner = () => {
  const { t } = useTranslation("common");

  const mapData = [
    {
      label: t("country-1-city"),
      cardPosition: ["top", "left", "line-up"],
    },
    {
      label: t("country-2-city"),
      cardPosition: ["bottom", "left", "line-down"],
    },
    {
      label: t("country-3-city"),
      cardPosition: ["top", "right", "line-down"],
    },
    {
      label: t("country-4-city"),
      cardPosition: ["bottom", "right", "line-up"],
    },
  ].map((item, i) => ({ ...item, id: i }));

  return (
    <div className={styles["map__img-wrap"]}>
      <img
        className="fix-img"
        src="/static/img/_general/map/map-1115.png"
        width="1115"
        alt=""
        srcSet="
          /static/img/_general/map/map-540.png 540w,
          /static/img/_general/map/map-720.png 720w,
          /static/img/_general/map/map-960.png 960w,
          /static/img/_general/map/map-1115.png 1115w,
        "
        sizes="
          (max-width: 767px) 540px,
          (max-width: 991px) 720px,
          (max-width: 1199px) 960px,
          1115px,
        "
      />
      <div className={styles.map__img__points}>
        {mapData.map((item) => (
          <div
            className={clsx(
              styles.map__img__points__item,
              styles[`item-${item.id + 1}`],
              styles[item.cardPosition[0]],
              styles[item.cardPosition[1]],
              styles[item.cardPosition[2]],
            )}
            key={item.id}
          >
            <div className={styles.map__img__points__item__point} />
            <div className={styles.map__img__points__item__line} />
            <div className={styles.map__img__points__item__card}>
              <span
                className={clsx(
                  styles["map__img__points__item__card-text"],
                  "c-text-gradient",
                )}
              >
                {item.label}
              </span>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};
