import React from "react";
import styles from "./Slider.module.scss";

export const Slide = ({ children }) => {
  return <div className={styles.slide}>{children}</div>;
};
