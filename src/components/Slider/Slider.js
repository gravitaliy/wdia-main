import React, { useState } from "react";
import clsx from "clsx";
import { Radio } from "../Radio/Radio";
import styles from "./Slider.module.scss";
// import { ArrowButton } from "../Buttons/ArrowButton/ArrowButton";

export const Slider = ({ children }) => {
  const [length] = useState(React.Children.count(children) - 1);
  const [count, setCount] = useState(0);

  const handleClick = (newCount) => {
    let c = newCount;
    if (newCount > length) c = 0;
    else if (newCount < 0) c = length;

    setCount(c);
  };

  return (
    <div className={styles.slider}>
      {React.Children.map(children, (child, i) => (i === count ? child : null))}
      <div className={clsx(styles["slider-footer"], "flex")}>
        {/*
        <div className={styles["arrow-buttons"]}>
          <ArrowButton onClick={() => handleClick(count - 1)} />
          <ArrowButton
            style={{ marginLeft: 10 }}
            onClick={() => handleClick(count + 1)}
            arrow="right"
          />
        </div>
        */}

        <div className={styles["radio-slider"]}>
          {Array.from({ length: length + 1 }, (_, i) => i).map((_, i) => (
            <Radio
              id={`slide- + ${i}`}
              handleChange={() => handleClick(i)}
              checked={i === count}
              key={i}
            />
          ))}
        </div>
      </div>
    </div>
  );
};
