import React from "react";
import styles from "./SearchInput.module.scss";

export const SearchInput = () => (
  <div className={styles["search-input-container"]}>
    <label htmlFor="{styles[search-input">
      <img
        src="/static/img/Header/search.svg"
        className={styles["search-input_search"]}
        alt="search"
      />
      <input
        id="search-input"
        className={styles["search-input"]}
        placeholder="Search"
      />
    </label>
  </div>
);
