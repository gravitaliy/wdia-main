import React from "react";
import InputMask from "react-input-mask";
import clsx from "clsx";
import styles from "./Input.module.scss";

export const Input = (props) => {
  return (
    <label
      className={clsx(styles["c-input__label"], props.classNameLabel)}
      htmlFor={props.id}
    >
      <span className={styles["c-input__label-text"]}>
        {props.label}
        {props.required ? "*" : ""}
      </span>
      {props.type === "textarea" ? (
        <textarea
          className={styles["c-input__input"]}
          id={props.id}
          name={props.name}
          placeholder={props.placeholder}
          onChange={props.onChange}
          required={!!props.required}
        />
      ) : (
        <InputMask
          mask={props.mask}
          maskChar=""
          value={props.value}
          onChange={props.onChange}
        >
          {() => (
            <input
              className={styles["c-input__input"]}
              type={props.type || "text"}
              name={props.name}
              id={props.id}
              onChange={props.onChange}
              placeholder={props.placeholder}
              required={!!props.required}
            />
          )}
        </InputMask>
      )}
    </label>
  );
};
