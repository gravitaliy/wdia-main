import React from "react";
import clsx from "clsx";
import styles from "./Button.module.scss";

/* eslint-disable react/button-has-type */
export const Button = ({
  text,
  children,
  onClick,
  type,
  typeButton,
  style,
  className,
  color,
}) => (
  <button
    type={type || "button"}
    onClick={onClick}
    style={style}
    className={clsx(
      styles.button,
      styles["button-default"],
      styles[typeButton],
      styles[color],
      styles[className],
    )}
  >
    {text || children}
  </button>
);
