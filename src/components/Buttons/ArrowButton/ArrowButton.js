import React from "react";
import styles from "./ArrowButton.module.scss";

export const ArrowButton = ({ arrow, onClick, style }) => {
  return (
    <button
      style={style}
      onClick={onClick}
      className={styles["arrow-button"]}
      type="button"
    >
      <img
        src={
          arrow === "right"
            ? "/static/img/_components/ArrowButton/arrow-right.png"
            : "/static/img/_components/ArrowButton/arrow-left.png"
        }
        alt="right"
      />
    </button>
  );
};
