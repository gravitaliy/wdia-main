import React from "react";
import styles from "./ButtonSubmit.module.scss";

export const ButtonSubmit = ({ value, onClick }) => {
  return (
    <input
      className={styles["submit-button"]}
      type="submit"
      value={value}
      onClick={onClick}
    />
  );
};
