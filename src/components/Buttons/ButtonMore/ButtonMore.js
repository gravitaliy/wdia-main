import React from "react";
import { useTranslation } from "react-i18next";
import clsx from "clsx";
import Link from "next/link";
import { ArrowSvg } from "./assets/ArrowSvg";
import styles from "./ButtonMore.module.scss";

export const ButtonMore = (props) => {
  const { t } = useTranslation("common");

  const styleLink = clsx(
    styles["button-more"],
    styles[props.color],
    styles[`hover-${props.hover}`],
    props.center && styles.center,
    styles[props.size],
  );

  const { forDynamicLinkItem } = props;

  if (forDynamicLinkItem) {
    return (
      <Link
        href={`${forDynamicLinkItem.href}[id]`}
        as={forDynamicLinkItem.href + forDynamicLinkItem.id}
      >
        <a
          className={styleLink}
          href={forDynamicLinkItem.href + forDynamicLinkItem.id}
        >
          <span>{props.title || t("btn-more")}</span>
          {props.arrow && <ArrowSvg />}
        </a>
      </Link>
    );
  }

  return (
    <a
      className={styleLink}
      href={props.href}
      target={props.target}
      rel={props.rel}
    >
      <span>{props.title || t("btn-more")}</span>
      {props.arrow && <ArrowSvg />}
    </a>
  );
};
