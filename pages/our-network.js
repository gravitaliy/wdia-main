import React from "react";
import { App } from "../src/App";
import { OurNetwork } from "../src/routes/OurNetwork/OurNetwork";

export default function page() {
  return (
    <App>
      <OurNetwork />
    </App>
  );
}
