import React from "react";
import { App } from "../src/App";
import { Main } from "../src/routes/Main/Main";

export default function page() {
  return (
    <App>
      <Main />
    </App>
  );
}
