import React from "react";
import { App } from "../src/App";
import { AboutUs } from "../src/routes/AboutUs/AboutUs";

export default function page() {
  return (
    <App>
      <AboutUs />
    </App>
  );
}
