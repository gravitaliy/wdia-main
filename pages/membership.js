import React from "react";
import { App } from "../src/App";
import { Membership } from "../src/routes/Membership/Membership";

export default function page() {
  return (
    <App>
      <Membership />
    </App>
  );
}
