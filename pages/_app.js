import React from "react";
import App from "next/app";
import Router from "next/router";
import NProgress from "nprogress";
import { appWithTranslation } from "../i18n";
import "nprogress/nprogress.css";
import "normalize.css/normalize.css";
import "../src/assets/styles/settings.scss";
import "rc-dropdown/assets/index.css";

Router.events.on("routeChangeStart", () => NProgress.start());
Router.events.on("routeChangeComplete", () => NProgress.done());
Router.events.on("routeChangeError", () => NProgress.done());

class MyApp extends App {
  render() {
    const { Component, pageProps } = this.props;

    return (
      // eslint-disable-next-line react/jsx-props-no-spreading
      <Component {...pageProps} />
    );
  }
}

export default appWithTranslation(MyApp);
