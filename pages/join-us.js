import React from "react";
import { App } from "../src/App";
import { JoinUs } from "../src/routes/JoinUs/JoinUs";

export default function page() {
  return (
    <App>
      <JoinUs />
    </App>
  );
}
