import React from "react";
import { App } from "../../src/App";
import { SingleArticle } from "../../src/routes/SingleArticle/SingleArticle";

export default function page() {
  return (
    <App>
      <SingleArticle />
    </App>
  );
}
