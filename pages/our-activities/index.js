import React from "react";
import { App } from "../../src/App";
import { OurActivities } from "../../src/routes/OurActivities/OurActivities";

export default function page() {
  return (
    <App>
      <OurActivities />
    </App>
  );
}
