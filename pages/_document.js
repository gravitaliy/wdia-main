/* eslint-disable react/no-danger */
import React from "react";
import Document, { Html, Head, Main, NextScript } from "next/document";

class MyDocument extends Document {
  render() {
    return (
      <Html>
        <Head />
        <body>
          <Main />
          <NextScript />
          <ExternalScripts />
        </body>
      </Html>
    );
  }
}

export default MyDocument;

const ExternalScripts = () => {
  return (
    <>
      {/* vk */}
      <script
        dangerouslySetInnerHTML={{
          __html: `
            !(function () {
              var t = document.createElement("script");
              (t.type = "text/javascript"),
                (t.async = !0),
                (t.src = "https://vk.com/js/api/openapi.js?166"),
                (t.onload = function () {
                  VK.Retargeting.Init("VK-RTRG-451950-2tp6t"), VK.Retargeting.Hit();
                }),
                document.head.appendChild(t);
            })();
          `,
        }}
      />
      <noscript>
        <img
          src="https://vk.com/rtrg?p=VK-RTRG-451950-2tp6t"
          style={{ position: "fixed", left: -999 }}
          alt=""
        />
      </noscript>

      {/* googletagmanager */}
      <script
        async
        src="https://www.googletagmanager.com/gtag/js?id=UA-154666310-1"
      />
      <script
        dangerouslySetInnerHTML={{
          __html: `
            window.dataLayer = window.dataLayer || [];
            function gtag() {
              dataLayer.push(arguments);
            }
            gtag("js", new Date());

            gtag("config", "UA-154666310-1");
          `,
        }}
      />


      {/* Yandex.Metrika counter */}
      <script
        dangerouslySetInnerHTML={{
          __html: `
            (function (m, e, t, r, i, k, a) {
              m[i] =
                m[i] ||
                function () {
                  (m[i].a = m[i].a || []).push(arguments);
                };
              m[i].l = 1 * new Date();
              (k = e.createElement(t)),
                (a = e.getElementsByTagName(t)[0]),
                (k.async = 1),
                (k.src = r),
                a.parentNode.insertBefore(k, a);
            })(
              window,
              document,
              "script",
              "https://mc.yandex.ru/metrika/tag.js",
              "ym",
            );

            ym(56693176, "init", {
              clickmap: true,
              trackLinks: true,
              accurateTrackBounce: true,
              webvisor: true,
            });
          `,
        }}
      />
      <noscript>
        <img
          style={{ position: "absolute", left: -9999 }}
          src="https://mc.yandex.ru/watch/56693176"
          alt=""
        />
      </noscript>

      {/* Facebook Pixel Code */}
      <script
        dangerouslySetInnerHTML={{
          __html: `
            !(function (f, b, e, v, n, t, s) {
              if (f.fbq) return;
              n = f.fbq = function () {
                n.callMethod
                  ? n.callMethod.apply(n, arguments)
                  : n.queue.push(arguments);
              };
              if (!f._fbq) f._fbq = n;
              n.push = n;
              n.loaded = !0;
              n.version = "2.0";
              n.queue = [];
              t = b.createElement(e);
              t.async = !0;
              t.src = v;
              s = b.getElementsByTagName(e)[0];
              s.parentNode.insertBefore(t, s);
            })(
              window,
              document,
              "script",
              "https://connect.facebook.net/en_US/fbevents.js",
            );
            fbq("init", "2858605747517418");
            fbq("track", "PageView");
          `,
        }}
      />
      <noscript>
        <img
          height="1"
          width="1"
          style={{ display: "none" }}
          src="https://www.facebook.com/tr?id=2858605747517418&ev=PageView&noscript=1"
          alt=""
        />
      </noscript>

      {/* Rating Mail.ru counter */}
      <script
        dangerouslySetInnerHTML={{
          __html: `
            var _tmr = window._tmr || (window._tmr = []);
            _tmr.push({
              id: "3160414",
              type: "pageView",
              start: new Date().getTime(),
              pid: "USER_ID",
            });
            (function (d, w, id) {
              if (d.getElementById(id)) return;
              var ts = d.createElement("script");
              ts.type = "text/javascript";
              ts.async = true;
              ts.id = id;
              ts.src = "https://top-fwz1.mail.ru/js/code.js";
              var f = function () {
                var s = d.getElementsByTagName("script")[0];
                s.parentNode.insertBefore(ts, s);
              };
              if (w.opera == "[object Opera]") {
                d.addEventListener("DOMContentLoaded", f, false);
              } else {
                f();
              }
            })(document, window, "topmailru-code");
          `,
        }}
      />
      <noscript>
        <div>
          <img
            src="https://top-fwz1.mail.ru/counter?id=3160414;js=na"
            style={{ boder: 0, position: "absolute", left: -9999 }}
            alt="Top.Mail.Ru"
          />
        </div>
      </noscript>
    </>
  );
};
