import React from "react";
import { App } from "../src/App";
import { AboutTech } from "../src/routes/AboutTech/AboutTech";

export default function page() {
  return (
    <App>
      <AboutTech />
    </App>
  );
}
